

import 'entity/notification_type.dart';
import 'entity/payment_method.dart';
import 'entity/voucher_method.dart';

class AppResource {
  AppResource._();

  static List<PaymentMethod> getPaymentMethod() {
    return List.of([
      PaymentMethod.cashOnDelivery,
      PaymentMethod.bankTransfer,
      PaymentMethod.eWallet
    ]);
  }

  static List<VoucherMethod> getVoucherMethod() {
    return List.of([VoucherMethod.voucher, VoucherMethod.rank]);
  }

  static List<NotificationType> getNotificationType() {
    return List.of([NotificationType.order, NotificationType.promotion]);
  }
}
