import 'package:bike_car/app/constants/style/style.dart';
import 'package:bike_car/app/constants/value/value.dart';
import 'package:bike_car/model/data_not_api/data_not_api_profile_customer.dart';
import 'package:bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class WidgetAppbarProfileTwo extends StatefulWidget {
  final String title;
  final List<Widget> left;
  final List<Widget> right;
  final Color indicatorColor;
  final Color backgroundColor;
  final Color textColor;
  final bool hasIndicator;

  WidgetAppbarProfileTwo(
      {Key key,
      this.title,
      this.left,
      this.right,
      this.indicatorColor,
      this.backgroundColor,
      this.hasIndicator = false,
      this.textColor,})
      : super(key: key);

  @override
  _WidgetAppbarProfileTwoState createState() => _WidgetAppbarProfileTwoState();
}

class _WidgetAppbarProfileTwoState extends State<WidgetAppbarProfileTwo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          // height: AppValue.ACTION_BAR_HEIGHT * 2,
          height: AppValue.ACTION_BAR_HEIGHT ,
          decoration: BoxDecoration(
              color: Color(0xff0bccd2),
          ),
          //padding: EdgeInsets.only(top: 15),
          child: Stack(
            // overflow: Overflow.visible,
            children: [
              widget.left != null
                  ? Positioned.fill(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        //crossAxisAlignment: CrossAxisAlignment.center,
                        children: widget.left,
                      ),
                    )
                  : SizedBox(),
              widget.right != null
                  ? Positioned.fill(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        //crossAxisAlignment: CrossAxisAlignment.center,
                        children: widget.right,
                      ),
                    )
                  : SizedBox(),
              widget.title != null
                  ? Positioned.fill(
                child: Center(
                  child: Text(
                    widget.title,
                    style: AppStyle.DEFAULT_MEDIUM.copyWith(
                        color: widget.textColor != null ? widget.textColor : Colors.black,
                        fontWeight: FontWeight.bold,fontSize: 22),
                  ),
                ),

                // child: FractionallySizedBox(
                      //     widthFactor: .8,
                      //     child: Container(
                      //       height: AppValue.ACTION_BAR_HEIGHT * 1.25,
                      //       child: Center(
                      //         child: Text(
                      //           widget.title,
                      //           style: AppStyle.DEFAULT_MEDIUM.copyWith(
                      //               color: widget.textColor != null ? widget.textColor : Colors.black,
                      //               fontWeight: FontWeight.bold),
                      //         ),
                      //       ),
                      //     )),
                    )
                  : SizedBox(),
            ],
          ),
        ),
        // widget.hasIndicator
        //     ? Divider(
        //         height: 1,
        //         thickness: 1,
        //         color: widget.indicatorColor != null
        //             ? widget.indicatorColor
        //             : Colors.grey,
        //       )
        //     : WidgetSpacer(
        //         height: 0,
        //       )
      ],
    );
  }

}


