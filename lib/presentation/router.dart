import 'package:bike_car/model/repo/barrel_repo.dart';
import 'package:bike_car/presentation/screen/forgot_password/sc_forgot_password.dart';
import 'package:bike_car/presentation/screen/forgot_password_reset/sc_forgot_password_reset.dart';
import 'package:bike_car/presentation/screen/forgot_password_verify/sc_forgot_password_verify.dart';
import 'package:bike_car/presentation/screen/login/sc_login.dart';
import 'package:bike_car/presentation/screen/menu/account/contact/sc_contact.dart';
import 'package:bike_car/presentation/screen/menu/account/delivery_address/sc_delivery_address.dart';
import 'package:bike_car/presentation/screen/menu/account/history_order/ic_history_order.dart';
import 'package:bike_car/presentation/screen/menu/account/installment/barrell_installment.dart';
import 'package:bike_car/presentation/screen/menu/account/profile_detail/sc_profile_detail_two.dart';
import 'package:bike_car/presentation/screen/menu/account/term/sc_term.dart';
import 'package:bike_car/presentation/screen/menu/home/detail_product/sc_detail_product.dart';
import 'package:bike_car/presentation/screen/menu/home/order/widget_order.dart';


import 'package:bike_car/presentation/screen/register/sc_register.dart';
import 'package:bike_car/presentation/screen/register_verify/sc_register_verify.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'screen/splash/sc_splash.dart';
import 'screen/work_navigation/work_navigation.dart';


class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String WORK_NAVIGATION = '/work_navigation';
  static const String FORGOT_PASSWORD = '/forgot_password';
  static const String FORGOT_PASSWORD_VERIFY = '/forgot_password_verify';
  static const String FORGOT_PASSWORD_RESET = '/forgot_password_reset';
  static const String REGISTER = '/register';
  static const String REGISTER_VERIFY = '/register_verify';
  static const String PROFILE_DETAIL = '/profile_detail';
  static const String TERM = '/term';
  static const String CONTACT = '/contact';
  static const String DELIVERY_ADDRESS = '/delivery_address';
  static const String DETAIL_PRODUCT = '/detail_product';
  static const String ADD_ODER = '/add_order';
  static const String HISTORY_ORDER = '/history_order';


  static const String INSTALLMENT = '/INSTALLMENT';



  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case LOGIN:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case WORK_NAVIGATION:
        return MaterialPageRoute(builder: (_) => WorkNavigationScreen());
      case CONTACT:
        return MaterialPageRoute(builder: (_) => ContactScreen());
      case INSTALLMENT:
        return MaterialPageRoute(builder: (_) => InstallmentScreen());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      LOGIN: (context) => LoginScreen(),
      WORK_NAVIGATION: (context) => WorkNavigationScreen(),
      FORGOT_PASSWORD: (context) => ForgotPasswordScreen(),
      FORGOT_PASSWORD_VERIFY: (context) => ForgotPasswordVerifyScreen(),
      FORGOT_PASSWORD_RESET: (context) => ForgotPasswordResetScreen(),
      REGISTER: (context) => RegisterScreen(),
      REGISTER_VERIFY: (context) => RegisterVerifyScreen(),
      PROFILE_DETAIL: (context) => ProfileDetailTwoPageScreen(),
      TERM: (context) => TermScreen(),
      CONTACT: (context) => ContactScreen(),
      DELIVERY_ADDRESS: (context) => DeliveryAddressPageScreen(),
      DETAIL_PRODUCT: (context) => DetailProductPageScreen(),
      ADD_ODER: (context) => WidgetOderPageScreen(),
      HISTORY_ORDER: (context) => HistoryOrderPageScreen(),



      INSTALLMENT: (context) => InstallmentScreen()

    };
  }
}
