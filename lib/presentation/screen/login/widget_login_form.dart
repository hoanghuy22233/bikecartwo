import 'package:bike_car/app/constants/color/color.dart';
import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/app/constants/string/validator.dart';
import 'package:bike_car/app/constants/style/style.dart';
import 'package:bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

class WidgetLoginForm extends StatefulWidget {
  @override
  _WidgetLoginFormState createState() => _WidgetLoginFormState();
}

class _WidgetLoginFormState extends State<WidgetLoginForm> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool obscurePassword = true;
  bool autoValidate = false;

  bool get isPopulated =>
      _usernameController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _usernameController.text = 'Letuanhuy98@gmail.com';
    _passwordController.text = '123456789';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 1.15,
            child: TextFormField(
              enableInteractiveSelection: false,
              controller: _usernameController,
              onChanged: (value) {
                // _loginBloc.add(LoginUsernameChanged(email: value));
              },
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,fontFamily: 'Roboto'),
              validator:
                  AppValidation.validateUserName("Vui lòng điền tài khoản"),
              decoration: InputDecoration(
                hintText: "Số điện thoại hoặc email",
                hintStyle: TextStyle(color: Colors.white),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
              ),
              // textAlign: TextAlign.start,
            ),
          ),
          WidgetSpacer(
            height: 10,
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.15,
            child: TextFormField(
              enableInteractiveSelection: false,
              controller: _passwordController,
              obscureText: obscurePassword,
              onChanged: (value) {
                // _loginBloc
                //     .add(LoginPasswordChanged(password: value));
              },
              validator:
                  AppValidation.validatePassword("Vui lòng nhập mật khẩu"),
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Mật khẩu",
                hintStyle: TextStyle(color: Colors.white),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                suffixIcon: IconButton(
                  icon: Icon(
                    obscurePassword
                        ? MaterialCommunityIcons.eye_outline
                        : MaterialCommunityIcons.eye_off_outline,
                    color: AppColor.WHITE,
                  ),
                  onPressed: () {
                    setState(() {
                      obscurePassword = !obscurePassword;
                    });
                  },
                ),
              ),
              style: TextStyle(color: Colors.white, fontSize: 16),
              textAlign: TextAlign.start,
            ),
          ), // Container(
          WidgetSpacer(
            height: 50,
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              padding: EdgeInsets.only(right: 20),
              child: GestureDetector(
                onTap: () {
                  AppNavigator.navigateForgotPassword();
                },
                child: Text(
                  'Quên Mật Khẩu?',
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.underline,fontFamily: 'Roboto'),
                ),
              ),
            ),
          ),
          WidgetSpacer(
            height: 15,
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 50,
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateWorkService();
              },
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                color: Color(0xff0bccd2),
                child: Center(
                    child: Text(
                  "Đăng nhập",
                  style: AppStyle.DEFAULT_MEDIUM.copyWith(
                      color: AppColor.WHITE, fontWeight: FontWeight.bold,fontFamily: 'Roboto'),
                )),
              ),
            ),
          ),
          // _buildButtonLogin(),
          WidgetSpacer(
            height: 30,
          ),
          GestureDetector(
            onTap: () {
              AppNavigator.navigateRegister();
            },
            child: Container(
              padding: EdgeInsets.only(bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/images/ic_person_add.png",
                    width: 20,
                    height: 20,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Đăng ký tài khoản',
                    style: TextStyle(fontSize: 18, color: Colors.white,fontFamily: 'Roboto'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  // _buildButtonLogin() {
  //   return ;
  // }

}
