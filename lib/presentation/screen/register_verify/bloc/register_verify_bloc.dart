import 'package:bike_car/app/constants/endpoint/app_endpoint.dart';
import 'package:bike_car/app/constants/preferences/app_preferences.dart';
import 'package:bike_car/model/local/pref.dart';
import 'package:bike_car/model/repo/user_repository.dart';
import 'package:bike_car/presentation/screen/register_verify/bloc/register_verify_event.dart';
import 'package:bike_car/presentation/screen/register_verify/bloc/register_verify_state.dart';
import 'package:bike_car/utils/validator/validator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class RegisterVerifyBloc
    extends Bloc<RegisterVerifyEvent, RegisterVerifyState> {
  final UserRepository _userRepository;

  RegisterVerifyBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  RegisterVerifyState get initialState => RegisterVerifyState.empty();

  @override
  Stream<RegisterVerifyState> mapEventToState(
      RegisterVerifyEvent event) async* {
    if (event is OtpCodeChanged) {
      yield* _mapOtpCodeChangeToState(event.otpCode);
    } else if (event is RegisterVerifySubmitted) {
      yield* _mapFormSubmittedToState(event.username, event.otpCode);
    }
  }

  Stream<RegisterVerifyState> _mapOtpCodeChangeToState(String otpCode) async* {
    yield state.update(isOtpCodeValid: Validator.isValidOtp(otpCode));
  }

  Stream<RegisterVerifyState> _mapFormSubmittedToState(
      String username, String otpCode) async* {
    yield RegisterVerifyState.loading();

    //need refactor
    var isOtpCodeValid = Validator.isValidOtp(otpCode);

    var newState = state.update(
      isOtpCodeValid: isOtpCodeValid,
    );

    yield newState;

    // if (newState.isFormValid) {
    //   try {
    //     var response = await _userRepository.registerVerify(
    //         username: username, otpCode: otpCode);
    //     if (response.status == Endpoint.SUCCESS) {
    //       final prefs = LocalPref();
    //       await prefs.saveBool(AppPreferences.new_register, true);
    //       yield RegisterVerifyState.success(message: response.message);
    //     } else {
    //       yield RegisterVerifyState.failure(message: response.message);
    //     }
    //   } catch (e) {
    //     print(e.toString());
    //     yield RegisterVerifyState.failure(message: e.toString());
    //   }
    // }
  }
}
