import 'package:bike_car/app/constants/color/color.dart';
import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/model/repo/user_repository.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:bike_car/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:bike_car/presentation/screen/forgot_password_reset/widget_forgot_password_reset_appbar.dart';
import 'package:bike_car/presentation/screen/forgot_password_reset/widget_forgot_password_reset_form.dart';
import 'package:bike_car/presentation/screen/forgot_password_reset/widget_forgot_password_reset_title.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/forgot_password_reset_bloc.dart';

class ForgotPasswordResetScreen extends StatefulWidget {
  @override
  _ForgotPasswordResetScreenState createState() =>
      _ForgotPasswordResetScreenState();
}

class _ForgotPasswordResetScreenState extends State<ForgotPasswordResetScreen> {
  String _username;
  String _otpCode;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _username = arguments['username'];
        _otpCode = arguments['otp_code'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xff0bccd2),
          body: BlocProvider(
            create: (context) =>
                ForgotPasswordResetBloc(userRepository: userRepository),
            child: Container(
                child: Column(
                  children: [
                    _buildAppbar(),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            WidgetSpacer(
                              height: 45,
                            ),
                            _buildTop(),
                            _buildForgotPasswordResetForm(),
                            WidgetSpacer(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff0bccd2),
    textColor: Colors.white,
    // title: "Công Việc",
    // right: [
    //   Padding(
    //     padding: EdgeInsets.only( top: 3,right: 10),
    //     child: GestureDetector(
    //       onTap: () {
    //         // showMaterialModalBottomSheet(
    //         //   shape: RoundedRectangleBorder(
    //         //     // borderRadius: BorderRadius.circular(10.0),
    //         //     borderRadius: BorderRadius.only(
    //         //         topLeft: Radius.circular(15.0),
    //         //         topRight: Radius.circular(15.0)),
    //         //   ),
    //         //   backgroundColor: Colors.white,
    //         //   context: context,
    //         //   builder: (context) => SeachWorkPageScreen(),
    //         // );
    //       },
    //       child: Image.asset(
    //         "assets/icons/phone-call.png",
    //         width: 25,
    //         height: 25,
    //         color: Colors.white,
    //       ),
    //     ),
    //   )
    // ],
    left: [
      Padding(
        padding: const EdgeInsets.only( left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          // onTap: () {
          //   showMaterialModalBottomSheet(
          //     shape: RoundedRectangleBorder(
          //       // borderRadius: BorderRadius.circular(10.0),
          //       borderRadius: BorderRadius.only(
          //           topLeft: Radius.circular(15.0),
          //           topRight: Radius.circular(15.0)),
          //     ),
          //     backgroundColor: Colors.white,
          //     context: context,
          //     builder: (context) => SeachDateWorkPageScreen(),
          //   );
          // },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );


  _buildTop() => WidgetForgotPasswordResetTitle();

  _buildForgotPasswordResetForm() => WidgetForgotPasswordResetForm(
        username: _username,
        otpCode: _otpCode,
      );
}
