import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher.dart';

import 'appbar_store.dart';
class StorePageScreen extends StatefulWidget {
  @override
  _StorePageScreenState createState() => _StorePageScreenState();
}

class _StorePageScreenState extends State<StorePageScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          StoreAppbar(),
          Container(
            height: MediaQuery.of(context).size.height*0.83,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(11),
            child: Card(
              child: Column(
                children: [
                  Image.asset("assets/images/logo_car_two.png"
                      , height: MediaQuery.of(context).size.height/3.5,),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 5),
                    child:
                    Divider(
                      color: Colors.grey,
                      thickness: 0.4,
                      height: 1,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top:30,bottom: 30),
                    child:
                      Text("Liên hệ với chúng tôi".toUpperCase(),
                      style: TextStyle(
                        fontSize: 14, fontStyle: FontStyle.italic, letterSpacing: 1.5,
                        fontWeight: FontWeight.bold
                      ),)
                  ),


                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: widgetMailAddress()
                  ),
                  SizedBox(height: 10,),
                  Padding(

                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: widgetAddress()
                  ),
                  SizedBox(height: 10,),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: widgetPhone()
                  ),
                  SizedBox(height: 10,),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: widgetWebsite()
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
widgetMailAddress(){
  return Row(
    children: [
      Container(
        width:30, height: 30,
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Color(0xff0bccd2),
          shape: BoxShape.circle,

        ),
        child: Image.asset("assets/images/ic_email.png", color: Colors.white,),
      ),
      SizedBox(width: 10,),
      GestureDetector(
          onTap: ()=> launch("mailto:temisvietnam@gmail.com"),
          child: Text("abcxyzt@gmail.com", style: TextStyle(color: Colors.black),))


    ],
  );
}
widgetAddress(){
  return Row(
    children: [
      Container(
        width:30, height: 30,
        padding: EdgeInsets.all(2),
        decoration: BoxDecoration(
          color: Color(0xff0bccd2),
          shape: BoxShape.circle,

        ),
        child: Image.asset("assets/images/iconfinder_location_1814106.png", color: Colors.white,),
      ),
      SizedBox(width: 10,),
      Flexible(child: Text("Shop xe 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM", style: TextStyle(color: Colors.black),))

    ],
  );
}
widgetPhone(){
  return Row(
    children: [
      Container(
        width:30, height: 30,
        padding: EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: Color(0xff0bccd2),
          shape: BoxShape.circle,

        ),
        child: Image.asset("assets/images/iconfinder_phone-call_2561306.png", color: Colors.white,),
      ),
      SizedBox(width: 10,),
      GestureDetector(
        onTap:()  => launch("tel://0888898000"),


            child: Text("0888898000", style: TextStyle(color: Colors.black, decoration: TextDecoration.underline),))

    ],
  );
}
widgetWebsite(){
  return Row(
    children: [
      Container(
        width:30, height: 30,
        padding: EdgeInsets.all(4),
        decoration: BoxDecoration(
          color: Color(0xff0bccd2),
          shape: BoxShape.circle,

        ),
        child: Image.asset("assets/images/iconfinder_world_www_web_website_5340283.png", fit: BoxFit.cover, color: Colors.white,),
      ),
      SizedBox(width: 10,),
      GestureDetector(
          onTap:() => launch("http://trumgop.com/"),
          child: Text("http://trumgop.com/",  style: TextStyle(decoration: TextDecoration.underline,color: Colors.black),))

    ],
  );
}

