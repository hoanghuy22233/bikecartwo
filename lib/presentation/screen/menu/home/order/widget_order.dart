import 'package:bike_car/app/constants/string/validator.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_certification.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_order_history.dart';
import 'package:flutter/material.dart';

import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:date_format/date_format.dart';

class WidgetOderPageScreen extends StatefulWidget {
  @override
  _WidgetOderPageScreenState createState() => _WidgetOderPageScreenState();
}

class _WidgetOderPageScreenState extends State<WidgetOderPageScreen> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
 List<DataListOrderHistory> orderHistory= [];
  // var now = DateTime.now();
  var dateTimeNow = formatDate(DateTime.now(), [dd, '/', mm, '/', yyyy, ' ', HH, ':', nn]);
  bool autoValidate = false;
  bool get isPopulated =>
      _usernameController.text.isNotEmpty &&
          _phoneNumberController.text.isNotEmpty;
  int id;
  // String _phone;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        id = arguments['id'];
        // _phone = arguments['phone'];
        print("_______________");
        print(id);
        // print(_phone);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _usernameController.text = 'Lê Tuấn Huy';
    _phoneNumberController.text = '0869258726';

    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            top: true,
            child: Column(
              children: [
                Stack(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 7,
                      color: Color(0xff0bccd2),
                      child: Column(
                        children: [
                          _buildAppbar(),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                      const EdgeInsets.only(top: 70, left: 15, right: 15),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 7,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        child: Row(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2.5,
                              height: MediaQuery.of(context).size.height,
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10)),
                                child: Image.asset(
                                  "${dataListCertification[id].img}",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 10, top: 30),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    "${dataListCertification[id].textNameCar}",
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    "${dataListCertification[id].textPrice} đ",
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: ListView(
                    children: [
                      Padding(
                        padding:
                        EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                    flex: 4,
                                    child: Text(
                                      "Năm sản xuất",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          color: Colors.grey),
                                    )),
                                SizedBox(
                                  width: 5,
                                ),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: Text(
                                    "Sản xuất ${dataListCertification[id].textYM}",
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  flex: 8,
                                  child: Text(
                                    "Động cơ (phân khối)",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: Colors.grey),
                                  ),
                                ),
                                SizedBox(
                                  width: 35,
                                ),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: Text(
                                    "${dataListCertification[id].textEngine.toString()} cc",
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: Text(
                                    "Màu sắc",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: Colors.grey),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: Container(
                                    width: 20,
                                    height: 20,
                                    decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(500)),
                                      color: dataListCertification[id].textColor,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 5,
                        color: Colors.grey[200],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20, top: 20, right: 20),
                        child: Text(
                          "THÔNG TIN NGƯỜI MUA ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 20),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20, top: 20, right: 20),
                        child: Text(
                          "Họ và tên: ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.grey,
                              fontSize: 18),
                        ),
                      ),
                      Padding(
                        padding:
                        EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 20),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 2,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(50)),
                              color: Colors.grey[400]),
                          child: Center(
                            child: TextFormField(
                              enableInteractiveSelection: false,
                              controller: _usernameController,
                              onChanged: (value) {
                                // _loginBloc.add(LoginUsernameChanged(email: value));
                              },
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Roboto',
                              ),
                              validator: AppValidation.validateUserName(
                                  "Vui lòng điền họ và tên"),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                                hintText: "Họ và tên",
                                hintStyle: TextStyle(color: Colors.white),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                              ),
                              // textAlign: TextAlign.start,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20, top: 10, right: 20),
                        child: Text(
                          "Số điện thoại: ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.grey,
                              fontSize: 18),
                        ),
                      ),
                      Padding(
                        padding:
                        EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 20),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 2,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(50)),
                              color: Colors.grey[400]),
                          child: Center(
                            child: TextFormField(
                              enableInteractiveSelection: false,
                              controller: _phoneNumberController,
                              onChanged: (value) {
                                // _loginBloc.add(LoginUsernameChanged(email: value));
                              },
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Roboto',
                              ),
                              validator: AppValidation.validateUserName(
                                  "Vui lòng điền số điện thoại"),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                                hintText: "Số điện thoại",
                                hintStyle: TextStyle(color: Colors.white),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                ),
                              ),
                              // textAlign: TextAlign.start,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 5,
                        color: Colors.grey[200],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20, top: 20, right: 20),
                        child: Text(
                          "THÔNG TIN THANH TOÁN ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 20),
                        ),
                      ),
                      Padding(
                        padding:
                        EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                    flex: 4,
                                    child: Text(
                                      "Số tiền cần trả",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          color: Colors.grey),
                                    )),
                                SizedBox(
                                  width: 5,
                                ),
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: Text(
                                    "${dataListCertification[id].textPrice} đ",
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: Text(
                                    "Hình thức thanh toán",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: Colors.grey),
                                  ),
                                ),

                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(1.0),
                                  child: Stack(
                                    children: [
                                      Container(
                                        width: 30,
                                        height: 30,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(500),
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                BorderRadius.circular(500),
                                                border: Border.all(
                                                    color: Color(0xff00b1b7), width: 1  )),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Container(
                                          width: 22,
                                          height: 22,
                                          decoration: BoxDecoration(
                                            color: Color(0xff00b1b7),
                                            borderRadius:
                                            BorderRadius.circular(500),

                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  "Tiền mặt",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.grey),
                                ),

                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 5,
                        color: Colors.grey[200],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 20, top: 20, right: 20),
                        child: Text(
                          "THÔNG TIN VẬN CHUYỂN ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 20),
                        ),
                      ),
                      Padding(
                        padding:
                        EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 10),
                        child: Text(
                          "Người mua cần đến trực tiếp cửa hàng để nhận xe",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                              color: Colors.grey),
                        ),
                      ),
                      Padding(
                        padding:
                        EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 20),
                        child:   Container(
                          width: MediaQuery.of(context).size.width-40,
                          height:  50,
                          child: GestureDetector(
                            onTap: () {
                              AppNavigator.navigateHistoryOder(

                                // ),
                                // orderHistory: id,
                                  // phone: _usernameController.text,
                                  // name: _phonenumberController.text
                              );
                              dataListOrderHistory.add(DataListOrderHistory(
                                img: "${dataListCertification[id].img}",
                                textNameCar: "${dataListCertification[id].textNameCar}",
                                textDateFull: dateTimeNow,
                                textPrice: "${dataListCertification[id].textPrice}",
                                textName: _usernameController.text,
                                textPhone: _phoneNumberController.text,
                                id: 0,
                              ));
                            },
                            child: Card(
                              elevation: 2,
                              shape:
                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                              color: Color(0xff00b1b7),
                              child: Center(
                                  child: Text(
                                    "Tiếp tục",
                                    style: TextStyle(color: Colors.white,fontSize: 16),
                                  )),
                            ),
                          ),
                        ),
                        // child: GestureDetector(
                        //   onTap: () {
                        //     AppNavigator.navigateHistoryOder(
                        //         id: id,
                        //         phone: _usernameController.text,
                        //         name: _phonenumberController.text);
                        //   },
                        //   child: Container(
                        //     width: MediaQuery.of(context).size.width / 2,
                        //     height: 45,
                        //     decoration: BoxDecoration(
                        //         borderRadius: BorderRadius.all(Radius.circular(50)),
                        //         color: Color(0xff0bccd2)),
                        //     child: Center(
                        //       child: Text(
                        //         "Gửi",
                        //         style: TextStyle(
                        //             color: Colors.white, fontWeight: FontWeight.bold),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                      ),
                    ],
                  ),
                )
              ],
            )
        ));
  }

  _buildAppbar() => WidgetAppbarProfile(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        title: "Đặt hàng",
        right: [
          // Padding(
          //   padding: EdgeInsets.only(top: 3, right: 10),
          //   child: GestureDetector(
          //     onTap: () {
          //       // showMaterialModalBottomSheet(
          //       //   shape: RoundedRectangleBorder(
          //       //     // borderRadius: BorderRadius.circular(10.0),
          //       //     borderRadius: BorderRadius.only(
          //       //         topLeft: Radius.circular(15.0),
          //       //         topRight: Radius.circular(15.0)),
          //       //   ),
          //       //   backgroundColor: Colors.white,
          //       //   context: context,
          //       //   builder: (context) => SeachWorkPageScreen(),
          //       // );
          //     },
          //     child: Image.asset(
          //       "assets/images/ic_fillter.png",
          //       width: 20,
          //       height: 20,
          //       color: Colors.white,
          //     ),
          //   ),
          // )
        ],
        left: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateBack();
              },
              child: Image.asset(
                "assets/images/ic_arrow.png",
                width: 20,
                height: 20,
                color: Colors.white,
              ),
            ),
          )        ],
      );
}
