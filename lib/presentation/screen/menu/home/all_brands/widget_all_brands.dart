import 'package:bike_car/model/data_not_api/data_not_api_list_popular_brand.dart';
import 'package:bike_car/presentation/screen/menu/home/all_brands/child_list_all_brands.dart';
import 'package:bike_car/presentation/screen/menu/home/all_brands/widget_all_brands_appbar.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class WidgetAllBrand extends StatefulWidget {
  @override

  _WidgetAllBrandState createState() => _WidgetAllBrandState();


}

class _WidgetAllBrandState extends State<WidgetAllBrand> {
  int _selectedIndex = 0;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SafeArea(
        child: Column(
          children: [
            WidgetAllBrandsAppbar(),
            Container(padding: EdgeInsets.all(5),
              width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height/13,

              child: Theme(
                data: ThemeData(
                  primaryColor: Color(0xff0bccd2),
                  primaryColorDark: Color(0xff0bccd2)
                ),
                child: TextField(
                  decoration: InputDecoration(

                    hintText: "Tìm kiếm thương hiệu",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff0bccd2)),

                      borderRadius: BorderRadius.all(

                          Radius.circular(15.0))
                    )
                  ),



                  ),
              ),
              ),
            Card(
              elevation: 5,
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "Thương hiệu phổ biến",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  SizedBox(height: 5,),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 7,
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: dataListPopularBrand.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.only(right: 15),
                            child: Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    // AppNavigator.navigateLogin();
                                    _onSelected(index);
                                  },
                                  child:                           Container(
                                    width: 100,
                                    height: 100,
                                    decoration: BoxDecoration(
                                        color: _selectedIndex != null &&
                                            _selectedIndex == index
                                            ? Color(0xff96e5e7)
                                            : Colors.grey[200],
                                        // boxShadow: [
                                        //   BoxShadow(color: Colors.green, spreadRadius: 5),
                                        // ],
                                        border: Border.all(  color: _selectedIndex != null &&
                                            _selectedIndex == index
                                            ? Colors.lightBlue
                                            : Colors.grey[200],width: 2),
                                        borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Image.asset(
                                          "${dataListPopularBrand[index].img}",
                                          width: 55,
                                          height: 55,
                                          // color: _selectedIndex != null &&
                                          //     _selectedIndex == index
                                          //     ? Colors.blueAccent[600]
                                          //     : Colors.grey[500],
                                        ),
                                        SizedBox(height: 5,),
                                        Text("${dataListPopularBrand[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold),),
                                      ],
                                    ),
                                  ),

                                ),
                                SizedBox(
                                  height: 10,
                                ),

                              ],
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Text(
                  "Tất cả thương hiệu",
                  style: TextStyle(
                      fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Expanded(child: Container(
              padding: EdgeInsets.symmetric(horizontal: 5),
              width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: ChildListAllBrands())),
            Container(
              padding: EdgeInsets.all(15),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height/10,
              child: TextButton(
                child: Text("Xác nhận", style: TextStyle(fontSize: 16),),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Color(0xff0bccd2)),
                    foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                ),
              ),
            )



          ],
        ),
      ),
    );
  }}