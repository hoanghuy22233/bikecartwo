import 'package:bike_car/model/data_not_api/data_not_api_list_all_brand.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChildListAllBrands extends StatefulWidget {
  @override
  _ChildListAllBrandsState createState() => _ChildListAllBrandsState();
}

class _ChildListAllBrandsState extends State<ChildListAllBrands> {
  int _selectedIndex = 0;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: GridView.builder(
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 1,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5

              ),

              itemCount: dataListAllBrand.length,
              itemBuilder: (context, index){
              return Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      // AppNavigator.navigateLogin();
                      _onSelected(index);
                    },
                    child:Container(
                      width: 120,
                      height: 120,
                      decoration: BoxDecoration(
                          color: _selectedIndex != null &&
                              _selectedIndex == index
                              ? Colors.lightBlue[300]
                              : Colors.grey[200],
                          borderRadius:
                          BorderRadius.all(Radius.circular(5))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            "${dataListAllBrand[index].img}",
                            width: 35,
                            height: 35,
                            color: _selectedIndex != null &&
                                _selectedIndex == index
                                ? Colors.blueAccent[600]
                                : Colors.grey[500],
                          ),
                          SizedBox(height: 10,),
                          Text("${dataListAllBrand[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold),),
                        ],
                      ),
                    ),

                  ),
                  SizedBox(
                    height: 10,
                  ),

                ],
              );


              },
            ),
          ),
        ],
      ),
    );
  }

}
