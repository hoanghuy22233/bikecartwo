import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_account.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_certification.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_popular_brand.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_product.dart';
import 'package:bike_car/presentation/screen/menu/account/profile/list_profile.dart';
import 'package:flutter/material.dart';

class WidgetAllProduct extends StatefulWidget {
  @override
  _WidgetAllProductState createState() => _WidgetAllProductState();
}

class _WidgetAllProductState extends State<WidgetAllProduct> {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final itemHeight =size.height/1.3;
    final itemWidth = size.width;
    return GridView.builder(
      // padding: EdgeInsets.only(top: 10, left: 5, right: 5),
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: (itemWidth/itemHeight),
      ),
      physics: NeverScrollableScrollPhysics(),
      itemCount: dataListCertification.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(right: 15,top: 10),
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  AppNavigator.detailProduct(id:index);
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 2.4,
                  // height: MediaQuery.of(context).size.height/2 ,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.height / 4.5,
                            height: MediaQuery.of(context).size.height / 5,
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5)),
                              child: Image.asset(
                                "${dataListCertification[index].img}",
                                width: 35,
                                height: 35,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Container(
                                margin:
                                EdgeInsets.only(right: 20, bottom: 20),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(500),
                                  child: Container(
                                    padding: EdgeInsets.all(10),
                                    height: 40,
                                    width: 40,
                                    color: Colors.blueGrey[400],
                                    // child: productAll[widget.id].like == 0
                                    //     ? Image.asset(
                                    //   "assets/images/heart.png",
                                    //   height: 15,
                                    //   width: 15,
                                    // )
                                    //     : Image.asset(
                                    //   "assets/images/favorites.png",
                                    //   height: 15,
                                    //   width: 15,
                                    // ),
                                    child: Image.asset(
                                      ("assets/icons/ic_heart_gray.png"),
                                      height: 15,
                                      width: 15,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 130),
                              child: dataListCertification[index].installment?Container(
                                width: 80,
                                height: 30,
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                  ),
                                ),
                                child: Center(
                                    child: Text(
                                      "Trả Góp",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                      fontSize: 12),
                                    )),
                              ):Container(),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${dataListCertification[index].textNameCar}",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${dataListCertification[index].textPrice}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 16),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Text(
                                  "${dataListCertification[index].textTime}",
                                  style: TextStyle(color: Colors.grey,fontSize: 12),
                                ),
                                Text(
                                  " - ",
                                  style: TextStyle(color: Colors.grey),
                                ),
                                Text(dataListCertification[index].textLocation.length>9?'${dataListCertification[index].textLocation.substring(0, 9)}...' : dataListCertification[index].textLocation,style: TextStyle(color: Colors.grey,fontSize: 12),)

                                // Text(
                                //   "${dataListCertification[index].textLocation}",
                                //   style: TextStyle(color: Colors.grey,fontSize: 12),
                                // ),
                              ],
                            ),
                            // SizedBox(
                            //   height: 5,
                            // ),
                            // Container(
                            //   width: 30,
                            //   height: 30,
                            //   child: ClipRRect(
                            //     borderRadius: BorderRadius.circular(500),
                            //     child: Image.asset(
                            //       "assets/images/logo_xe.jpg",
                            //       fit: BoxFit.cover,
                            //     ),
                            //   ),
                            // )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );


    // return Container(
    //   width: MediaQuery.of(context).size.width,
    //   height: MediaQuery.of(context).size.height / 2.5,
    //   child: ListView.builder(
    //       shrinkWrap: true,
    //       scrollDirection: Axis.horizontal,
    //       itemCount: dataListCertification.length,
    //       itemBuilder: (BuildContext context, int index) {
    //         return Padding(
    //           padding: const EdgeInsets.only(right: 15),
    //           child: Column(
    //             children: [
    //               SizedBox(
    //                 height: 10,
    //               ),
    //               GestureDetector(
    //                 onTap: () {},
    //                 child: Container(
    //                   width: MediaQuery.of(context).size.height / 4.5,
    //                   height: MediaQuery.of(context).size.height / 2.8,
    //                   decoration: BoxDecoration(
    //                       color: Colors.grey[200],
    //                       borderRadius: BorderRadius.all(Radius.circular(5))),
    //                   child: Column(
    //                     mainAxisAlignment: MainAxisAlignment.start,
    //                     crossAxisAlignment: CrossAxisAlignment.start,
    //                     children: [
    //                       Stack(
    //                         children: [
    //                           Container(
    //                             width: MediaQuery.of(context).size.height / 4.5,
    //                             height: MediaQuery.of(context).size.height / 5,
    //                             child: ClipRRect(
    //                               borderRadius: BorderRadius.only(
    //                                   topLeft: Radius.circular(5),
    //                                   topRight: Radius.circular(5)),
    //                               child: Image.asset(
    //                                 "${dataListCertification[index].img}",
    //                                 width: 35,
    //                                 height: 35,
    //                                 fit: BoxFit.cover,
    //                               ),
    //                             ),
    //                           ),
    //                           Align(
    //                             alignment: Alignment.topRight,
    //                             child: Padding(
    //                               padding: const EdgeInsets.only(top: 10),
    //                               child: Container(
    //                                 margin:
    //                                     EdgeInsets.only(right: 20, bottom: 20),
    //                                 child: ClipRRect(
    //                                   borderRadius: BorderRadius.circular(500),
    //                                   child: Container(
    //                                     padding: EdgeInsets.all(10),
    //                                     height: 40,
    //                                     width: 40,
    //                                     color: Colors.blueGrey[400],
    //                                     // child: productAll[widget.id].like == 0
    //                                     //     ? Image.asset(
    //                                     //   "assets/images/heart.png",
    //                                     //   height: 15,
    //                                     //   width: 15,
    //                                     // )
    //                                     //     : Image.asset(
    //                                     //   "assets/images/favorites.png",
    //                                     //   height: 15,
    //                                     //   width: 15,
    //                                     // ),
    //                                     child: Image.asset(
    //                                       ("assets/icons/ic_heart_gray.png"),
    //                                       height: 15,
    //                                       width: 15,
    //                                       color: Colors.white,
    //                                     ),
    //                                   ),
    //                                 ),
    //                               ),
    //                             ),
    //                           ),
    //                           Align(
    //                             alignment: Alignment.topRight,
    //                             child: Padding(
    //                               padding: const EdgeInsets.only(top: 130),
    //                               child: Container(
    //                                 width: 60,
    //                                 height: 30,
    //                                 decoration: BoxDecoration(
    //                                   color: Colors.blue,
    //                                   borderRadius: BorderRadius.only(
    //                                     topLeft: Radius.circular(10),
    //                                   ),
    //                                 ),
    //                                 child: Center(
    //                                     child: Text(
    //                                   "Trả Góp",
    //                                   style: TextStyle(
    //                                       fontWeight: FontWeight.bold,
    //                                       color: Colors.white),
    //                                 )),
    //                               ),
    //                             ),
    //                           ),
    //                         ],
    //                       ),
    //                       SizedBox(
    //                         height: 10,
    //                       ),
    //                       Padding(
    //                         padding: EdgeInsets.symmetric(horizontal: 10),
    //                         child: Column(
    //                           mainAxisAlignment: MainAxisAlignment.start,
    //                           crossAxisAlignment: CrossAxisAlignment.start,
    //                           children: [
    //                             Text(
    //                               "${dataListCertification[index].textNameCar}",
    //                               style: TextStyle(fontWeight: FontWeight.bold),
    //                             ),
    //                             SizedBox(
    //                               height: 5,
    //                             ),
    //                             Text(
    //                               "${dataListCertification[index].textPrice}",
    //                               style: TextStyle(
    //                                   fontWeight: FontWeight.bold,
    //                                   color: Colors.black,
    //                                   fontSize: 16),
    //                             ),
    //                             SizedBox(
    //                               height: 5,
    //                             ),
    //                             Row(
    //                               children: [
    //                                 Text(
    //                                   "${dataListCertification[index].textTime}",
    //                                   style: TextStyle(color: Colors.grey),
    //                                 ),
    //                                 Text(
    //                                   " - ",
    //                                   style: TextStyle(color: Colors.grey),
    //                                 ),
    //                                 Text(
    //                                   "${dataListCertification[index].textLocation}",
    //                                   style: TextStyle(color: Colors.grey),
    //                                 ),
    //                               ],
    //                             ),
    //                             SizedBox(
    //                               height: 5,
    //                             ),
    //                             Container(
    //                               width: 30,
    //                               height: 30,
    //                               child: ClipRRect(
    //                                 borderRadius: BorderRadius.circular(500),
    //                                 child: Image.asset(
    //                                   "assets/images/logo_xe.jpg",
    //                                   fit: BoxFit.cover,
    //                                 ),
    //                               ),
    //                             )
    //                           ],
    //                         ),
    //                       )
    //                     ],
    //                   ),
    //                 ),
    //               ),
    //             ],
    //           ),
    //         );
    //       }),
    // );
  }
}
