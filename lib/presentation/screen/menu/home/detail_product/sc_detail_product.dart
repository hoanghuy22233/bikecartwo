import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_certification.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile_two.dart';
import 'package:bike_car/presentation/screen/menu/home/detail_product/widget_bottom_detail_product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class DetailProductPageScreen extends StatefulWidget {
  @override
  _DetailProductPageScreenState createState() =>
      _DetailProductPageScreenState();
}

class _DetailProductPageScreenState extends State<DetailProductPageScreen> {
  int id;
  // String _phone;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        id = arguments['id'];
        // _phone = arguments['phone'];
        print("_______________");
        print(id);
        // print(_phone);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Column(
            children: [
              _buildAppbar(),
              Expanded(child: ListView(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 2,
                    child: Swiper(
                      itemBuilder: (BuildContext context, int index) {
                        return new Image.asset(
                          "${dataListCertification[id].img}",
                          fit: BoxFit.fill,
                        );
                      },
                      autoplay: false,
                      itemCount: 3,
                      pagination: new SwiperPagination(
                        // margin: new EdgeInsets.all(0.0),
                          builder: new SwiperCustomPagination(builder:
                              (BuildContext context, SwiperPluginConfig config) {
                            return new ConstrainedBox(
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Text(
                                  " ${config.activeIndex + 1}/${config.itemCount}",
                                  style: new TextStyle(fontSize: 16.0, color: Colors.white),
                                ),
                              ),
                              constraints: new BoxConstraints.expand(
                                height: 30.0,
                              ),
                            );
                          })),
                      control: new SwiperControl(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, top: 20, bottom: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              "${dataListCertification[id].textNameCar}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,
                                  color: Colors.grey),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "${dataListCertification[id].textNameCar}",
                              style: TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "${dataListCertification[id].textPrice} đ",
                          style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              "Giá thị trường: ",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.black),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "${dataListCertification[id].textPML} ~ ${dataListCertification[id].textPMR}đ",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.blue,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 3,
                    color: Colors.grey[300],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                            flex: 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Stack(
                                    children: [
                                      Container(
                                        width: 54,
                                        height: 54,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.all(Radius.circular(500)),
                                            color: Colors.white,
                                            border: Border.all(
                                                color: Colors.grey, width: 2)),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: Container(
                                          width: 52,
                                          height: 52,
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.circular(500),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.black,
                                                  borderRadius:
                                                  BorderRadius.circular(500),
                                                  border: Border.all(
                                                      color: Colors.white, width: 3)),
                                              child: Padding(
                                                padding: EdgeInsets.all(6.0),
                                                child: AspectRatio(
                                                  aspectRatio: 1,
                                                  child: Center(
                                                      child: dataListCertification[id].status? Text(
                                                        "Used",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 12,
                                                            fontWeight: FontWeight.bold),
                                                      ):Text(
                                                        "New",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 12,
                                                            fontWeight: FontWeight.bold),
                                                      )
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "Tình trạng",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14,
                                    color: Colors.black,
                                  ),
                                )
                              ],
                            )),
                        Container(
                          height: 50,
                          width: 1,
                          color: Colors.grey,
                        ),
                        Expanded(
                            flex: 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 15),
                                  child: Image.asset(
                                    "assets/images/ic_clutch_disc.png",
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Text(
                                    "${dataListCertification[id].textVehicleType}",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                      color: Colors.black,
                                    ),
                                  ),
                                )
                              ],
                            )),
                        Container(
                          height: 50,
                          width: 1,
                          color: Colors.grey,
                        ),
                        Expanded(
                            flex: 3,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 40),
                                  child: Image.asset(
                                    "assets/images/ic_calendar.png",
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Row(
                                    children: [
                                      Text(
                                        "Năm sử dụng: ",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        "${dataListCertification[id].textYearUse}",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.lightBlue,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )),
                        Container(
                          height: 50,
                          width: 1,
                          color: Colors.grey,
                        ),
                        Expanded(
                            flex: 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 20),
                                  child: Image.asset(
                                    "assets/images/ic_speed_ometer.png",
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 7),
                                  child: Text(
                                    "${dataListCertification[id].textKm.toStringAsFixed(3)} km",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                      color: Colors.black,
                                    ),
                                  ),
                                )
                              ],
                            )),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 5,
                    color: Colors.grey[300],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, top: 20, bottom: 20,right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                                flex: 4,
                                child: Text(
                                  "Năm sản xuất",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.grey),
                                )),
                            SizedBox(
                              width: 5,
                            ),
                            Align(alignment: Alignment.bottomRight,
                              child: Text(
                                "Sản xuất ${dataListCertification[id].textYM}",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 8,
                              child: Text(
                                "Động cơ (phân khối)",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.grey),
                              ),
                            ),
                            SizedBox(
                              width: 35,
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Text(
                                "${dataListCertification[id].textEngine.toString()} cc",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 5,
                              child: Text(
                                "Màu sắc",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.grey),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(500)),
                                  color: dataListCertification[id].textColor,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 5,
                              child: Text(
                                "Hotline",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.grey),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Text(
                                "${dataListCertification[id].textPhone}",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,decoration: TextDecoration.underline),
                              ),
                            ),


                          ],
                        ),
                        // SizedBox(
                        //   height: 10,
                        // ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, top: 30, bottom: 20,right: 10),
                    child: WidgetBottomDetailProduct(id:id),
                  ),
                ],
              )),
            ],
          )),
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
        backgroundColor: Color(0xff0bccd2),
        textColor: Colors.white,
        title: "${dataListCertification[id].textNameCar}",
        right: [
          // Padding(
          //   padding: EdgeInsets.only(top: 3, right: 10),
          //   child: GestureDetector(
          //     onTap: () {
          //       // showMaterialModalBottomSheet(
          //       //   shape: RoundedRectangleBorder(
          //       //     // borderRadius: BorderRadius.circular(10.0),
          //       //     borderRadius: BorderRadius.only(
          //       //         topLeft: Radius.circular(15.0),
          //       //         topRight: Radius.circular(15.0)),
          //       //   ),
          //       //   backgroundColor: Colors.white,
          //       //   context: context,
          //       //   builder: (context) => SeachWorkPageScreen(),
          //       // );
          //     },
          //     child: Image.asset(
          //       "assets/icons/nav_bell.png",
          //       width: 25,
          //       height: 25,
          //       color: Colors.white,
          //     ),
          //   ),
          // )
        ],
        left: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateBack();
              },
              // onTap: () {
              //   showMaterialModalBottomSheet(
              //     shape: RoundedRectangleBorder(
              //       // borderRadius: BorderRadius.circular(10.0),
              //       borderRadius: BorderRadius.only(
              //           topLeft: Radius.circular(15.0),
              //           topRight: Radius.circular(15.0)),
              //     ),
              //     backgroundColor: Colors.white,
              //     context: context,
              //     builder: (context) => SeachDateWorkPageScreen(),
              //   );
              // },
              child: Image.asset(
                "assets/images/ic_arrow.png",
                width: 20,
                height: 20,
                color: Colors.white,
              ),
            ),
          )
        ],
      );
}
