import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:flutter/material.dart';
class WidgetBottomDetailProduct extends StatefulWidget {
  final int id;
  WidgetBottomDetailProduct({Key key, this.id}) : super(key: key);

  @override
  _WidgetBottomDetailProductState createState() => _WidgetBottomDetailProductState();
}

class _WidgetBottomDetailProductState extends State<WidgetBottomDetailProduct> {
  @override
  Widget build(BuildContext context) {
    var _selected = 0;
    // List<Tab> rows = [];
    return Container(
      decoration: BoxDecoration(
          color: Color(0xff0bccd2),
          borderRadius: BorderRadius.all(Radius.circular(500))),
      child: DefaultTabController(
        length: 2,
        child: TabBar(
          unselectedLabelColor: Colors.white,
          indicatorSize: TabBarIndicatorSize.tab,
          indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(500),
              color: Color(0xFFFF8900)),
          tabs: [
            Tab(child: Text("Chia sẻ"),),
            Tab(child: Text("Đặt hàng"),),
          ],
          onTap: (value) {
            setState(() {
              // _selected = value;
              if(_selected==value && _selected==0){
                // AppNavigator.navigateLogin();
              }else{
                AppNavigator.navigateAddOder(id:widget.id);
              }
            });
          },
        ),
      ),
    );
  }


}
