import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_account.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_product.dart';
import 'package:bike_car/presentation/screen/menu/account/profile/list_profile.dart';
import 'package:flutter/material.dart';

class WidgetListProduct extends StatefulWidget {
  @override
  _WidgetListProductState createState() => _WidgetListProductState();
}

class _WidgetListProductState extends State<WidgetListProduct> {
  int _selectedIndex = 0;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 7,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: dataListProduct.length,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return Padding(
                padding: const EdgeInsets.only(right: 15),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        // AppNavigator.navigateLogin();
                        _onSelected(index);
                      },
                      child: Stack(
                        children: [
                          Container(
                            width: 60,
                            height: 60,
                            decoration: BoxDecoration(
                                color: _selectedIndex != null &&
                                    _selectedIndex == index
                                    ? Color(0xffccf2f3)
                                    : Colors.grey[300],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(500))),
                            child: Image.asset(
                              "${dataListProduct[index].img}",
                              width: 35,
                              height: 35,
                                color: _selectedIndex != null &&
                                    _selectedIndex == index
                                    ? Color(0xff0dc0b8)
                                    : Colors.grey[500],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text("Tất cả"),
                  ],
                ),
              );
            }
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () {
                      // AppNavigator.navigateLogin();
                      _onSelected(index);
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                              color: _selectedIndex != null &&
                                      _selectedIndex == index
                                  ? Color(0xffccf2f3)
                                  : Colors.grey[200],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(500))),
                          child: Image.asset(
                            "${dataListProduct[index].img}",
                            color: _selectedIndex != null &&
                                    _selectedIndex == index
                                ? Colors.blueAccent[600]
                                : Colors.grey[500],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text("${dataListProduct[index].textForm}")
                ],
              ),
            );
          }),
    );
  }
}
