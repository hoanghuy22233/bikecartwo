import 'dart:io';

import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class WidgetBackgroundImageThree extends StatefulWidget {
  @override
  _WidgetBackgroundImageThreeState createState() =>
      _WidgetBackgroundImageThreeState();
}

class _WidgetBackgroundImageThreeState
    extends State<WidgetBackgroundImageThree> {
  final _picker = ImagePicker();
  PickedFile avatarFile;
  File croppedFile;
  @override
  Widget build(BuildContext context) {
    return                 Column(
      children: [
        Stack(
          children: [
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 5,
                // color: Colors.grey,
                child: Image.asset("assets/images/logo_xe.jpg",
                    fit: BoxFit.cover)),
            Positioned(
              bottom: 10,
              right: 10,
              child: GestureDetector(
                onTap: () {
                  _buildSheetMethod(context);
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) =>
                  //           BackgroundImagePageScreen()),
                  // );
                },
                child: Container(
                  width: 40,
                  height: 40,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(500),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color(0xFFD6D5D5),
                          borderRadius: BorderRadius.circular(500),
                          border: Border.all(
                              color: Colors.white, width: 3)),
                      child: Padding(
                        padding: EdgeInsets.all(6.0),
                        child: AspectRatio(
                          aspectRatio: 1,
                          child: Image.asset(
                            'assets/images/img_camera2.png',
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 50,
              left: 5,
              child: GestureDetector(
                  onTap: () {
                    AppNavigator.navigateBack();
                  },
                  child: Image.asset(
                    'assets/images/ic_arrow.png',
                    color: Colors.white,
                    width: 20,
                    height: 20,
                  )),
            )
          ],
        ),
      ],
    )
    ;
  }
  _buildSheetMethod(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return CupertinoActionSheet(
            title: Text('Ảnh nền'),
            cancelButton: CupertinoActionSheetAction(
              child: Text('Huỷ'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            actions: [
              CupertinoActionSheetAction(
                onPressed: () {
                  _onPickAvatar();
                },
                child: Text('Chọn ảnh có sẵn'),
              ),
              CupertinoActionSheetAction(
                onPressed: () {
                  _onTakeAvatar();
                },
                child: Text('Chụp ảnh mới'),
              )
            ],
          );
        });
  }
  Future<Null> _onPickAvatar() async {
    avatarFile = null;
    avatarFile =
    await _picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    croppedFile = File(avatarFile.path);
    // if (avatarFile != null) {
    //   BlocProvider.of<ProfileDetailAvatarBloc>(context)
    //       .add(ProfileDetailAvatarUploadEvent(avatarFile: croppedFile)
    //   );
    // }
  }

  Future<Null> _onTakeAvatar() async {
    avatarFile = null;
    avatarFile =
    await _picker.getImage(source: ImageSource.camera, imageQuality: 25);
    croppedFile = File(avatarFile.path);
    if (avatarFile != null) {
      avatarFile: croppedFile;
    }
  }


}
