import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:flutter/material.dart';

class WidgetLogoContact extends StatelessWidget {
  final double widthPercent;
  final double height;
  final bool small;

  WidgetLogoContact({Key key, this.widthPercent, this.height, this.small = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(right: 40,top: 10),
            height: height ?? (AppValue.ACTION_BAR_HEIGHT * 1.5),
            child: Center(
              child: FractionallySizedBox(
                widthFactor: widthPercent ?? 0.4,
                child: Image.asset(small
                    ? 'assets/images/logo_car_two.png'
                    : 'assets/images/logo_car_two.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
