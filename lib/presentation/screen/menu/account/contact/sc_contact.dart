import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:bike_car/presentation/screen/menu/account/contact/widget_contact_detail.dart';
import 'package:bike_car/presentation/screen/menu/account/contact/widget_logo_contact.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

import 'barrel_contact.dart';

class ContactScreen extends StatefulWidget {
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen>
    with AutomaticKeepAliveClientMixin<ContactScreen> {
  @override
  void initState() {
    super.initState();
    // BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  var logger = Logger(
    printer: PrettyPrinter(),
  );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Container(color: AppColor.PRIMARY_COLOR, child: _buildContent()),
      ),
    );
  }

  _buildContent() {
    return Column(
      children: [_buildAppbar(), Expanded(child: _buildMenu())],
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff0bccd2),
    textColor: Colors.white,
    title: "Liên hệ",
    right: [
      // Padding(
      //   padding: EdgeInsets.only(top: 3, right: 10),
      //   child: GestureDetector(
      //     onTap: () {
      //       // showMaterialModalBottomSheet(
      //       //   shape: RoundedRectangleBorder(
      //       //     // borderRadius: BorderRadius.circular(10.0),
      //       //     borderRadius: BorderRadius.only(
      //       //         topLeft: Radius.circular(15.0),
      //       //         topRight: Radius.circular(15.0)),
      //       //   ),
      //       //   backgroundColor: Colors.white,
      //       //   context: context,
      //       //   builder: (context) => SeachWorkPageScreen(),
      //       // );
      //     },
      //     child: Image.asset(
      //       "assets/icons/ic_add.png",
      //       width: 20,
      //       height: 20,
      //       color: Colors.white,
      //     ),
      //   ),
      // )
    ],
    left: [
      Padding(
        padding: const EdgeInsets.only(left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          // onTap: () {
          //   showMaterialModalBottomSheet(
          //     shape: RoundedRectangleBorder(
          //       // borderRadius: BorderRadius.circular(10.0),
          //       borderRadius: BorderRadius.only(
          //           topLeft: Radius.circular(15.0),
          //           topRight: Radius.circular(15.0)),
          //     ),
          //     backgroundColor: Colors.white,
          //     context: context,
          //     builder: (context) => SeachDateWorkPageScreen(),
          //   );
          // },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );

  Widget _buildMenu() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.symmetric(
            vertical: AppValue.APP_HORIZONTAL_PADDING),
        child: Column(
          children: [
            // WidgetSpacer(
            //   height: 10,
            // ),
            _buildLogo(),
            // WidgetSpacer(
            //   height: 10,
            // ),
            _buildContactDetail(),
          ],
        ),
      ),
    );
  }

  Widget _buildContactDetail() => WidgetContactDetail();

  Widget _buildLogo() => WidgetLogoContact(
      height: MediaQuery.of(context).size.height / 4, widthPercent: 1);

  @override
  bool get wantKeepAlive => true;
}
