import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
class WidgetContactDetail extends StatelessWidget {
  WidgetContactDetail({Key key}) : super(key: key);
  // final _url = 'tel:0869258726';
  // final Uri _emailLaunchUri = Uri(
  //     scheme: 'mailto',
  //     path: 'letuanhuy98@gmail.com',
  //     queryParameters: {
  //       'subject': 'Example Subject & Symbols are allowed!'
  //     }
  // );
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          // Text(
          //   "SHOPXECUONGK.COM",
          //   textAlign: TextAlign.center,
          //   style: TextStyle(fontWeight: FontWeight.bold),
          // ),
          // SizedBox(
          //   height: 15,
          // ),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "assets/icons/ic_address.png",
                    width: 25,
                    height: 25,
                  )),
              Expanded(
                  flex: 9,
                  child: Text(": 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",style: TextStyle(color: Colors.blue,),))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                  // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0888898000");

                    // _launchURL();
                  },
                  child: Text('0888898000',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0901056662");

                    // _launchURL();
                  },
                  child: Text('0901056662',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0778889000");

                    // _launchURL();
                  },
                  child: Text('0778889000',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0939921691");

                    // _launchURL();
                  },
                  child: Text('0939921691',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0707703955");

                    // _launchURL();
                  },
                  child: Text('0707703955',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0388551054");

                    // _launchURL();
                  },
                  child: Text('0388551054',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0888840000");

                    // _launchURL();
                  },
                  child: Text('0888840000',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0338000040");

                    // _launchURL();
                  },
                  child: Text('0338000040',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0988226276");

                    // _launchURL();
                  },
                  child: Text('0988226276',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_internet.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9, child: GestureDetector(
                onTap: () {
                  launch("http://shopxecuongk.com/");
                },
                  child: Text("http://shopxecuongk.com/",style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),)))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_gmail.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                  child: GestureDetector(onTap: () {
                    launch("mailto:temisvietnam@gmail.com?subject=News&body=New%20plugin");
                  },child: Text("temisvietnam@gmail.com ",style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),))
              )
            ],
          ),
        ],
      ),
    );
  }
  // void _launchURL() async =>
  //     await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
  // // void _launchURLEmail() async =>
  // //     await canLaunch(_urlEmail) ? await launch(_urlEmail) : throw 'Could not launch $_urlEmail';
}
