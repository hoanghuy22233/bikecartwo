
import 'package:url_launcher/url_launcher.dart';
import 'package:bike_car/model/data_not_api/data_not_api_list_delivery_address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChildDeliveryAddress extends StatefulWidget {
  final int id;
  ChildDeliveryAddress({Key key, this.id}) : super(key: key);

  @override
  _ChildDeliveryAddressState createState() => _ChildDeliveryAddressState();
}

class _ChildDeliveryAddressState extends State<ChildDeliveryAddress> {
  final double tabBarHeight = 80;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
            // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Expanded(flex: 1,child: Container()),
                SizedBox(width: 5,),
                Expanded(flex: 1,child: Image.asset("${dataListDelivery[widget.id].imgAddress}",width: 30,height: 30,)),
                SizedBox(width: 5,),
                Expanded(flex: 7,child: Text("${dataListDelivery[widget.id].textAddress}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),)),
                Expanded(flex: 2,child: Container()),

              ],
            ),
          ),
    Row(
            mainAxisAlignment: MainAxisAlignment.start,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Expanded(flex: 1,child: Container()),
              SizedBox(width: 5,),
              Expanded(flex: 1,child: Image.asset("${dataListDelivery[widget.id].imgPhone}",width: 30,height: 30,)),
              SizedBox(width: 5,),
              Expanded(flex: 7,child: GestureDetector(
                onTap: () {
                  launch("tel:${dataListDelivery[widget.id].textPhone}");
                },
                child: Text("${dataListDelivery[widget.id].textPhone}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,decoration: TextDecoration.underline,
                ),),
              )),
              Expanded(flex: 2,child: Container()),

            ],
          ),

        ],
      ),
    );
  }
}
