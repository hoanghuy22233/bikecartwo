import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/app/constants/style/style.dart';
import 'package:bike_car/model/data_not_api/data_not_api_profile_customer.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile_two.dart';
import 'package:bike_car/presentation/screen/menu/account/delivery_address/widget_delivery_address_form.dart';
import 'package:bike_car/presentation/screen/menu/account/profile/widget_profile_form.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DeliveryAddressPageScreen extends StatefulWidget {
  @override
  _DeliveryAddressPageScreenstate createState() => _DeliveryAddressPageScreenstate();
}

class _DeliveryAddressPageScreenstate extends State<DeliveryAddressPageScreen> {
  // int _selectedIndex = 0;
  // _onSelected(int index) {
  //   setState(() => _selectedIndex = index);
  // }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Column(
        children: [
          _buildAppbar(),
          SizedBox(
            height: 10,
          ),
          Expanded(child: WidgetDeliveryAddressForm()),
        ],
      )),
    );
  }
}

_buildAppbar() => WidgetAppbarProfile(
      backgroundColor: Color(0xff0bccd2),
      textColor: Colors.white,
      title: "Địa chỉ nhận hàng",
      right: [
        Padding(
          padding: EdgeInsets.only(top: 3, right: 10),
          child: GestureDetector(
            onTap: () {
              // showMaterialModalBottomSheet(
              //   shape: RoundedRectangleBorder(
              //     // borderRadius: BorderRadius.circular(10.0),
              //     borderRadius: BorderRadius.only(
              //         topLeft: Radius.circular(15.0),
              //         topRight: Radius.circular(15.0)),
              //   ),
              //   backgroundColor: Colors.white,
              //   context: context,
              //   builder: (context) => SeachWorkPageScreen(),
              // );
            },
            child: Image.asset(
              "assets/icons/ic_add.png",
              width: 20,
              height: 20,
              color: Colors.white,
            ),
          ),
        )
      ],
      left: [
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: GestureDetector(
            onTap: () {
              AppNavigator.navigateBack();
            },
            // onTap: () {
            //   showMaterialModalBottomSheet(
            //     shape: RoundedRectangleBorder(
            //       // borderRadius: BorderRadius.circular(10.0),
            //       borderRadius: BorderRadius.only(
            //           topLeft: Radius.circular(15.0),
            //           topRight: Radius.circular(15.0)),
            //     ),
            //     backgroundColor: Colors.white,
            //     context: context,
            //     builder: (context) => SeachDateWorkPageScreen(),
            //   );
            // },
            child: Image.asset(
              "assets/images/ic_arrow.png",
              width: 20,
              height: 20,
              color: Colors.white,
            ),
          ),
        )
      ],
    );

// Widget _buildAppbar() => WidgetProfileAppbar();
