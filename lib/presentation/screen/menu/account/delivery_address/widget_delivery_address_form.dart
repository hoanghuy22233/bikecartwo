
import 'package:bike_car/model/data_not_api/data_not_api_list_delivery_address.dart';
import 'package:bike_car/presentation/screen/menu/account/delivery_address/list_delivery_address.dart';
import 'package:flutter/material.dart';

class WidgetDeliveryAddressForm extends StatefulWidget {
  @override
  _WidgetDeliveryAddressFormState createState() => _WidgetDeliveryAddressFormState();
}

class _WidgetDeliveryAddressFormState extends State<WidgetDeliveryAddressForm> {
  int _selectedIndex = 0;

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        child: Expanded(
            child: ListView.builder(
          itemCount: dataListDelivery.length,
              itemBuilder: (context, index) => Container(
                height: MediaQuery.of(context).size.height/8,
                color: _selectedIndex != null && _selectedIndex == index
                    ? Colors.grey[200]
                    : Colors.white,
                child: ListTile(
                  title: ChildDeliveryAddress(
                    id: index,
                  ),
                  onTap: () => _onSelected(index),
                ),
              ),


        )

            ),
      ),
    );
  }
}
