
import 'package:bike_car/model/data_not_api/data_not_api_profile_customer.dart';
import 'package:bike_car/presentation/screen/menu/account/avatar_three/widget_profile_detail_avatar_three.dart';
import 'package:bike_car/presentation/screen/menu/account/background_image_three/widget_background_image_three.dart';
import 'package:flutter/material.dart';


class ProfileDetailTwoPageScreen extends StatefulWidget {
  @override
  _ProfileDetailTwoPageScreenState createState() =>
      _ProfileDetailTwoPageScreenState();
}

class _ProfileDetailTwoPageScreenState
    extends State<ProfileDetailTwoPageScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Column(
        children: [
          // _buildAppbar(),
          Container(
            child: Stack(
              children: [
                WidgetBackgroundImageThree(),
                WidgetProfileDetailAvatarThree(),
              ],
            ),
          ),

          SizedBox(
            height: 30,
          ),

          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Họ và tên",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "${dataProfileCustomer[0].textForm}",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Số điện thoại",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "${dataProfileCustomer[0].textPhone}",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Email",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "${dataProfileCustomer[0].textEmail}",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Ngày sinh",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "${dataProfileCustomer[0].textBirthday}",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),

        ],
      )),
    );
  }
}

// Widget _buildAppbar() => WidgetProfileDetailAppbar();
