import 'dart:io';

import 'package:bike_car/model/data_not_api/data_not_api_profile_customer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class WidgetProfileDetailAvatarThree extends StatefulWidget {
  @override
  _WidgetProfileDetailAvatarThreeState createState() =>
      _WidgetProfileDetailAvatarThreeState();
}

class _WidgetProfileDetailAvatarThreeState
    extends State<WidgetProfileDetailAvatarThree> {
  final _picker = ImagePicker();
  PickedFile avatarFile;
  File croppedFile;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 110),
      child: Align(
        alignment: Alignment.center,
        child: Stack(
          children: [
            Container(
              width: 90,
              height: 90,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(300.0),
                child: Image.asset("assets/images/logo_xe.jpg", fit: BoxFit.cover
                  // width: 30,
                  // height: 30,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 60, top: 60),
              child: GestureDetector(
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) => AvatarTwoPageScreen()),
                  // );
                  _buildSheetMethod(context);
                },
                child: Container(
                  width: 35,
                  height: 35,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(500),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color(0xFFD6D5D5),
                          borderRadius: BorderRadius.circular(500),
                          border: Border.all(color: Colors.white, width: 3)),
                      child: Padding(
                        padding: EdgeInsets.all(6.0),
                        child: AspectRatio(
                          aspectRatio: 1,
                          child: Image.asset(
                            'assets/images/img_camera2.png',
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    // return Padding(
    //   padding: EdgeInsets.symmetric(horizontal: 150, vertical: 110),
    //   child: Stack(
    //     children: [
    //       Container(
    //         width: 90,
    //         height: 90,
    //         child: ClipRRect(
    //           borderRadius: BorderRadius.circular(300.0),
    //           child: Image.asset("assets/images/img_nongnghiep.jpg", fit: BoxFit.cover
    //               // width: 30,
    //               // height: 30,
    //               ),
    //         ),
    //       ),
    //       Padding(
    //         padding: EdgeInsets.only(left: 60, top: 60),
    //         child: GestureDetector(
    //           onTap: () {
    //             // Navigator.push(
    //             //   context,
    //             //   MaterialPageRoute(
    //             //       builder: (context) => AvatarTwoPageScreen()),
    //             // );
    //             _buildSheetMethod(context);
    //           },
    //           child: Container(
    //             width: 35,
    //             height: 35,
    //             child: ClipRRect(
    //               borderRadius: BorderRadius.circular(500),
    //               child: Container(
    //                 decoration: BoxDecoration(
    //                     color: Color(0xFFD6D5D5),
    //                     borderRadius: BorderRadius.circular(500),
    //                     border: Border.all(color: Colors.white, width: 3)),
    //                 child: Padding(
    //                   padding: EdgeInsets.all(6.0),
    //                   child: AspectRatio(
    //                     aspectRatio: 1,
    //                     child: Image.asset(
    //                       'assets/images/img_camera2.png',
    //                       fit: BoxFit.fitWidth,
    //                     ),
    //                   ),
    //                 ),
    //               ),
    //             ),
    //           ),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
  _buildSheetMethod(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return CupertinoActionSheet(
            title: Text('Ảnh đại diện'),
            cancelButton: CupertinoActionSheetAction(
              child: Text('Huỷ'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            actions: [
              CupertinoActionSheetAction(
                onPressed: () {
                  _onPickAvatar();
                },
                child: Text('Chọn ảnh có sẵn'),
              ),
              CupertinoActionSheetAction(
                onPressed: () {
                  _onTakeAvatar();
                },
                child: Text('Chụp ảnh mới'),
              )
            ],
          );
        });
  }
  Future<Null> _onPickAvatar() async {
    avatarFile = null;
    avatarFile =
    await _picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    croppedFile = File(avatarFile.path);
    // if (avatarFile != null) {
    //   BlocProvider.of<ProfileDetailAvatarBloc>(context)
    //       .add(ProfileDetailAvatarUploadEvent(avatarFile: croppedFile)
    //   );
    // }
  }

  Future<Null> _onTakeAvatar() async {
    avatarFile = null;
    avatarFile =
    await _picker.getImage(source: ImageSource.camera, imageQuality: 25);
    croppedFile = File(avatarFile.path);
    if (avatarFile != null) {
      avatarFile: croppedFile;
    }
  }


}
