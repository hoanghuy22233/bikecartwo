import 'package:bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:bike_car/presentation/screen/menu/news/news_bike/widget_news_bike.dart';
import 'package:bike_car/presentation/screen/menu/news/news_car/widget_news_car.dart';
import 'package:flutter/material.dart';
import 'package:bike_car/presentation/screen/menu/news/barrel_news..dart';
import 'package:bike_car/presentation/screen/menu/news/news_sim/widget_news_sim.dart';
class NewsPageScreen extends StatefulWidget {
  final int id;

  const NewsPageScreen({Key key, this.id}) : super(key: key);
  @override
  _NewsPageScreenState createState() => _NewsPageScreenState();
}

class _NewsPageScreenState extends State<NewsPageScreen> with SingleTickerProviderStateMixin {
  final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 3);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            NewsAppbar(),
            Container(
              height: MediaQuery.of(context).size.height/15,
              child: TabBar(
                physics: BouncingScrollPhysics(),
                labelColor: Color(0xff0bccd2),
                controller: _tabController,
                unselectedLabelColor: Colors.black,
                tabs: [
                  Container(
                    width: MediaQuery.of(context).size.width/3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset("assets/images/motorcycle.png", width: 25,height: 25,),
                        Text("Xe máy", ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset("assets/images/sports-car.png", width: 25,height: 25,),
                        Text("Ô tô", ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset("assets/images/sim (1).png", width: 25,height: 25,),
                        Text("Sim thẻ", ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: [
                  WidgetNewsBike(),
                  WidgetNewsCar(),
                  WidgetNewsSim(),
                ],
              ),

            )




          ],
        ),
      ),
    );
  }
}
