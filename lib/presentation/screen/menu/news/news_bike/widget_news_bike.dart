
import 'package:bike_car/model/data_not_api/data_not_api_bike_news.dart';
import 'package:bike_car/presentation/screen/menu/news/news_bike/list_news_bike.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetNewsBike extends StatefulWidget {
  const WidgetNewsBike({
    Key key,
  }) : super(key: key);
  @override
  _WidgetNewsBikeState createState() => _WidgetNewsBikeState();
}

class _WidgetNewsBikeState extends State<WidgetNewsBike>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: dataBikeNews.length,
                itemBuilder: (context, index) {
                  return Container(

                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 3.9,
                    child: Card(
                      elevation: 5,
                        child: ListNewsBike(id:index,)),
                  );
                }),
          )
        ],
      ),
    );
  }
}
