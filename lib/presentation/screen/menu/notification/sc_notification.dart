import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:bike_car/model/data_not_api/data_not_api_notification.dart';




class NotificationPageScreen extends StatefulWidget {
  @override
  _NotificationPageScreenState createState() => _NotificationPageScreenState();
}

class _NotificationPageScreenState extends State<NotificationPageScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
              child: ListView.builder(
                  itemCount: dataNotification.length,
                  itemBuilder: (context, index) {
                    return StickyHeader(

                      header: Container(
                        height: 50.0,
                        color:  Colors.white,
                        // decoration: BoxDecoration(
                        //   // color: Colors.blue,
                        //   border: Border(
                        //     top: BorderSide( //                   <--- left side
                        //       color: Colors.grey[400],
                        //       width: 2,
                        //     ),
                        //     bottom: BorderSide( //                    <--- top side
                        //       color: Colors.grey[400],
                        //       width: 2,
                        //     ),
                        //   ),
                        // ),
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        alignment: Alignment.centerLeft,
                        // child: Text('Header #$index',
                        //   style: const TextStyle(color: Colors.white),
                        // ),
                        child: Text("${dataNotification[index].textDateFull}",
                          style: const TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
                        ),
                      ),
                      content:                 Container(
                        color: Color(0xff77e8ec),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 50,horizontal: 10),

                              child: Container(
                                width: 40,
                                height: 40,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(300.0),
                                  child: Image.asset(
                                      "${dataNotification[index].img}",
                                      fit: BoxFit.cover
                                    // width: 30,
                                    // height: 30,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Container(
                                width: 2,
                                height: 130,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              flex: 7,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  RichText(text: TextSpan(
                                    text: "${dataNotification[index].name}",
                                    style: TextStyle(fontSize:20, fontWeight: FontWeight.bold, color: Colors.black, decoration: TextDecoration.none),
                                    children: [
                                      TextSpan(text: " ${dataNotification[index].textForm}",
                                      style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal))
                                    ]
                                  )
                                  ),


                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Image.asset("${dataNotification[index].imgDate}",width: 20, height: 20,),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        "${dataNotification[index].textDate}",
                                        style: const TextStyle(
                                          fontSize: 14,
                                          color: Colors.black87,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Colors.blue,
    textColor: Colors.white,
    title: "THÔNG BÁO",
    // right: [
    //   Padding(
    //     padding: EdgeInsets.only( top: 3,right: 10),
    //     child: GestureDetector(
    //       onTap: () {
    //         // showMaterialModalBottomSheet(
    //         //   shape: RoundedRectangleBorder(
    //         //     // borderRadius: BorderRadius.circular(10.0),
    //         //     borderRadius: BorderRadius.only(
    //         //         topLeft: Radius.circular(15.0),
    //         //         topRight: Radius.circular(15.0)),
    //         //   ),
    //         //   backgroundColor: Colors.white,
    //         //   context: context,
    //         //   builder: (context) => SeachWorkPageScreen(),
    //         // );
    //       },
    //       child: Image.asset(
    //         "assets/images/ic_fillter.png",
    //         width: 20,
    //         height: 20,
    //         color: Colors.white,
    //       ),
    //     ),
    //   )
    // ],
    left: [
      Padding(
        padding: const EdgeInsets.only( left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();

          },
          child: Image.asset(
            "assets/images/ic_back_two.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );
}
