import 'package:bike_car/app/constants/navigator/navigator.dart';
import 'package:bike_car/presentation/common_widgets/widget_link.dart';
import 'package:bike_car/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetBottomLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Text(
              AppLocalizations.of(context).translate('register.have_account'),style: TextStyle(color: Colors.white,fontSize: 18),
            ),
          ),
          Flexible(
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateLogin();
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                child: WidgetLink(
                  text:
                      AppLocalizations.of(context).translate('register.login'),style: TextStyle(color: Colors.white,fontSize:18,decoration: TextDecoration.underline,),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
