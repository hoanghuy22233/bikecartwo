import 'package:bike_car/app/constants/style/style.dart';
import 'package:bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:bike_car/utils/common/common_utils.dart';
import 'package:bike_car/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetForgotPasswordVerifyUsername extends StatelessWidget {
  final String username;

  const WidgetForgotPasswordVerifyUsername({Key key, @required this.username})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          'Nhập mã được gửi tới',
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,color: Colors.white),
        ),
        WidgetSpacer(
          height: 15,
        ),
        Text(
          '${AppCommonUtils.hideUserName("username")}',
          style: TextStyle(fontSize: 16,color: Colors.white),
        ),
      ],
    );
  }
}
