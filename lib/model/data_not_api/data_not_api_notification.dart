
class DataNotification {
  final String textForm,textDate,textDateFull, img,imgDate, name;
  final int id;
  DataNotification({
    this.name,
    this.textForm,
    this.textDate,
    this.textDateFull,
    this.img,
    this.imgDate,
    this.id,
  });
}

List<DataNotification> dataNotification = [
  DataNotification(
    name: "Xe máy yamaha exciter 150",
    textForm: "đang được vận chuyển ",
    textDate: "2 giờ trước",
    textDateFull: "Thứ 3, 09/03/2021",
    img: "assets/images/user1.png",
    imgDate:"assets/images/clock-circular-outline.png",
    id: 0,
  ),
  DataNotification(
    name: "Xe máy yamaha exciter 150",
    textForm: "đang được vận chuyển ",
    textDate: "2 giờ trước",
    textDateFull: "Thứ 3, 09/03/2021",
    img: "assets/images/user1.png",
    imgDate:"assets/images/clock-circular-outline.png",
    id: 1,
  ),
  DataNotification(
    name: "Xe máy yamaha exciter 150",
    textForm: "đang được vận chuyển ",
    textDate: "2 giờ trước",
    textDateFull: "Thứ 3, 09/03/2021",
    img: "assets/images/user1.png",
    imgDate:"assets/images/clock-circular-outline.png",
    id: 2,
  ),
  DataNotification(
    name: "Xe máy yamaha exciter 150",
    textForm: "đang được vận chuyển ",
    textDate: "2 giờ trước",
    textDateFull: "Thứ 4, 09/03/2021",
    img: "assets/images/user1.png",
    imgDate:"assets/images/clock-circular-outline.png",
    id: 3,
  ),





];
