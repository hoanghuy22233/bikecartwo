
class DataProfileCustomer {
  final String textForm,textLocated,textDate,textDateFull,textBottom, img ,textPhone,textEmail,textBirthday;
  final int id;
  DataProfileCustomer({
    this.textForm,
    this.textDate,
    this.textLocated,
    this.textBottom,
    this.textDateFull,
    this.textPhone,
    this.textEmail,
    this.textBirthday,
    this.img,
    this.id,
  });
}

List<DataProfileCustomer> dataProfileCustomer = [
  DataProfileCustomer(
    textForm: "Nguyễn Bùi Tuấn Anh",
    textLocated:"Trụ sở chính",
    textDate: "11/3",
    textDateFull: "08:19",
    textPhone: "033796881",
    textEmail: "buituananh97@gmail.com",
    textBirthday:"25/5/1997",
    textBottom:"Chúc bạn ngày làm việc hiệu quả.",
    img: "assets/images/image_avartar_girl_two.png",
    id: 0,
    // imgDate:"assets/images/date_off_three.png",
  ),
 


];
