
class DataListDelivery {
  final String textAddress,textPhone,imgAddress,imgPhone ;
  final int id;
  DataListDelivery({
    this.textAddress,
    this.textPhone,
    this.imgAddress,
    this.imgPhone,
    this.id,
  });
}

List<DataListDelivery> dataListDelivery = [
  DataListDelivery(
    textAddress: "Số 115/10 Hồ Sen - Hải Phòng",
    textPhone: "0983174332",
    imgAddress:"assets/images/address.png",
    imgPhone: "assets/icons/phone-call.png",
    id: 0, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListDelivery(
    textAddress: "Shop Xe 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",
    textPhone: "0888898000",
    imgAddress:"assets/images/address.png",
    imgPhone: "assets/icons/phone-call.png",
    id: 1, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListDelivery(
    textAddress: "Shop Xe 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",
    textPhone: "0878888850",
    imgAddress:"assets/images/address.png",
    imgPhone: "assets/icons/phone-call.png",
    id: 2, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListDelivery(
    textAddress: "Shop Xe 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",
    textPhone: "0888898000",
    imgAddress:"assets/images/address.png",
    imgPhone: "assets/icons/phone-call.png",
    id: 3, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListDelivery(
    textAddress: "Shop Xe 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",
    textPhone: "0878888850",
    imgAddress:"assets/images/address.png",
    imgPhone: "assets/icons/phone-call.png",
    id: 4, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListDelivery(
    textAddress: "Shop Xe 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",
    textPhone: "0888898000",
    imgAddress:"assets/images/address.png",
    imgPhone: "assets/icons/phone-call.png",
    id: 5, // imgDate:"assets/images/date_off_three.png",
  ),
  DataListDelivery(
    textAddress: "Shop Xe 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",
    textPhone: "0878888850",
    imgAddress:"assets/images/address.png",
    imgPhone: "assets/icons/phone-call.png",
    id: 6, // imgDate:"assets/images/date_off_three.png",
  ),

];
