import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends Equatable {
  int id;
  String email;
  String name;
  String tel;
  String address;
  String image;
  String gender;
  @JsonKey(name: "api_token")
  String apiToken;
  @JsonKey(name: "job")
  String job;
  @JsonKey(name: "intro")
  String intro;
  String birthday;
  @JsonKey(name: "role_display_name")
  String roleDisplayName;
  @JsonKey(name: "role_name")
  String roleName;


  User(
      this.id,
      this.email,
      this.name,
      this.tel,
      this.address,
      this.image,
      this.gender,
      this.apiToken,
      this.job,
      this.intro,
      this.birthday,
      this.roleDisplayName,
      this.roleName);




  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, tel: $tel, address: $address, image: $image, gender: $gender, birthday: $birthday, apiToken: $apiToken,job: $job, intro: $intro, gender: $roleDisplayName, birthday: $roleName,}';
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  List<Object> get props => [
    id,
    email,
    image,
    tel,
    name,
    apiToken,
    address,
    gender,
    job,
    intro,
    birthday,
    roleDisplayName,
    roleName
      ];
}
