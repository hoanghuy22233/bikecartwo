import 'dart:io';

import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';


import 'request/barrel_request.dart';
import 'response/barrel_response.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: Endpoint.BASE_URL)
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST(Endpoint.LOGIN_APP)
  Future<LoginRegisterResponse> loginApp(
      @Body() LoginAppRequest loginAppRequest);

}
