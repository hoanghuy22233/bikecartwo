import 'package:bike_car/app/constants/barrel_constants.dart';
import 'package:dio/dio.dart';


class DioProvider {
  static final Dio _dio = Dio();
  static Dio instance(String token) {
    _dio
      ..options.baseUrl = Endpoint.BASE_URL
      ..options.connectTimeout = Endpoint.connectionTimeout
      ..options.receiveTimeout = Endpoint.receiveTimeout
      ..options.headers = {
        AppNetWork.content_type: AppNetWork.multipart_form_data,
        AppNetWork.auth_type: AppNetWork.bearer(token)
      }
      ..interceptors.add(LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
        requestHeader: true,
      ));
    return _dio;
  }

  static void bearer(String token) {
    _dio
      ..options.headers = {
        AppNetWork.content_type: AppNetWork.multipart_form_data,
        AppNetWork.auth_type: AppNetWork.bearer(token)
      };
  }
}


