import 'dart:io';

import 'package:bike_car/model/api/request/barrel_request.dart';
import 'package:bike_car/model/api/response/barrel_response.dart';
import 'package:bike_car/model/api/rest_client.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class UserRepository {
  final Dio dio;

  UserRepository({@required this.dio});
  // final GoogleSignIn _googleSignIn = GoogleSignIn();
  //final facebookLogin = FacebookLogin();

  Future<LoginRegisterResponse> loginApp(
      {@required String username,
      @required String password,
      }) async {
    final client = RestClient(dio);
    return client.loginApp(LoginAppRequest(
        email: username, password: password));
  }



}
