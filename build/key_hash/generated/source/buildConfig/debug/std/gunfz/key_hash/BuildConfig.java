/**
 * Automatically generated file. DO NOT MODIFY
 */
package std.gunfz.key_hash;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "std.gunfz.key_hash";
  public static final String BUILD_TYPE = "debug";
}
